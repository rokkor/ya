package me.pixka.ya.w

import me.pixka.ya.d.*
import me.pixka.ya.task.LoadTjobTask
import me.pixka.ya.task.TaskService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class TEstLoadTjob {

    @Autowired
    lateinit var ts:TaskService

    @Autowired
    lateinit var ss:SensorService

    @Autowired
    lateinit var svs:SensorValueService

    @Autowired
    lateinit var jts:JobtypeService

    @Autowired
    lateinit var js:JobService

    @Autowired
    lateinit var lt:LoadTjobTask

    fun addSv(s:Sensor)
    {
        var sv = SensorValue()
        sv.t = 100.0
        sv.sensor=s
        svs.save(sv)

    }
    fun addJob()
    {
        var jt = jts.findOrCreate("t")
        println(jt)
        var job = Job()
        job.sensor = ss.findOrCreate("s1")
        job.name = "for test"
        job.tl = 0.0
        job.th = 100.0
        job.jobtype = jt
        job.jobtype_id = jt.id
       var j =  js.save(job)

        println(j.jobtype_id)
    }
    @Test
    fun testLoadtorun()
    {
//        var jt = jts.findOrCreate("t")
        var s = ss.findOrCreate("s1")
        addSv(s)
        addJob()
        var jobs = js.findByTypeid(1)
        println(jobs)
        lt.run()
//        Assertions.assertNotNull(lt)
    }
}