package me.pixka.ya.w

import me.pixka.ya.d.EmailforNotify
import me.pixka.ya.d.EmailforNotifyService
import me.pixka.ya.d.Job
import me.pixka.ya.s.LastService
import me.pixka.ya.task.TaskService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class TestTWorker {

    @Autowired
    lateinit var ts: TaskService

    @Autowired
    lateinit var ls: LastService

    @Autowired
    lateinit var es: EmailforNotifyService


    @Test
    fun testTworker() {
        var job = Job()
        job.name = "xx"
        job.id = 1
        job.tl = 15.0
        job.th = 100.0
        var t = TWorker(job, ls, es, 1L, "test")
        ts.run(t)


    }
}