package me.pixka.ya.s

import me.pixka.ya.c.Foradddata
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.util.*

@SpringBootTest
class TestLastService {
    @Autowired
    lateinit var ls: LastService


    @Test
    fun testLast() {

        var fa1 = Foradddata()
        fa1.valuedate = Date().time
        fa1.sensorid = "1"
        fa1.h = 100.0
        fa1.t = 10.20
        ls.add(fa1)
        var fa2 = Foradddata()
        fa2.sensorid="2"
        fa2.valuedate = Date().time
        fa2.h = 52.0
        fa2.t = 22.20
        ls.add(fa2)

        fa2 = Foradddata()
        fa2.sensorid="2"
        fa2.valuedate = Date().time
        fa2.h = 99.99
        fa2.t = 99.99
        ls.add(fa2)

        var last = ls.getLastupdate()

        println(last)
        Assertions.assertTrue(last.size>0)
    }
}