package me.pixka.ya.s

import me.pixka.ya.c.Foradddata
import me.pixka.ya.d.Sensor
import me.pixka.ya.d.SensorGroup
import me.pixka.ya.d.SensorGroupService
import me.pixka.ya.d.SensorService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestLastByGroup {

    @Autowired
    lateinit var ls: LastService

    @Autowired
    lateinit var ss: SensorService

    @Autowired
    lateinit var sgs: SensorGroupService
    fun addSensor(n: String = "defaultSensor", g: SensorGroup): Sensor {

        var s = ss.findOrCreate(n)
        s.sensorgroup = g
        s.sensorgroup_id = g.id
        return ss.save(s)
    }

    fun addSensorgroup(n: String = "default"): SensorGroup {

        return sgs.findOrCreate(n)
    }

    @Test
    fun testlistByGroup() {

        var g1 = addSensorgroup("g1")
        var g2 = addSensorgroup("g2")

        var s1 = addSensor("s1", g1)
        var s2 = addSensor("s2", g1)
        var s3 = addSensor("s3", g2)
        var s4 = addSensor("s4", g2)

        var f = Foradddata(s1.id,"",1000.0,10.0,null,s1)
        ls.add(f)
        f = Foradddata(s2.id,"",1000.0,10.0,null,s2)
        ls.add(f)
        f = Foradddata(s3.id,"",1000.0,10.0,null,s3)
        ls.add(f)
        f = Foradddata(s4.id,"",1000.0,10.0,null,s4)
        ls.add(f)
        var g1s = ls.findByGroup(g2.id)
        println(g1s)
    }

}