package me.pixka.ya.s

import me.pixka.ya.task.TaskService
import me.pixka.ya.w.BaseWorker
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.util.concurrent.TimeUnit

@SpringBootTest
class TestIsRunTask {
    @Autowired
    lateinit var ts:TaskService



    @Test
    fun testIsRun()
    {
        var w1 = BaseWorker(1L,"worker1",2L)
        var w2 = BaseWorker(2L,"worker2",0L)
        var w3 = BaseWorker(1L,"Worker 3")
        var w4 = BaseWorker(4L,"Worker 4",4L)
        ts.run(w1)
        ts.run(w2)
        ts.run(w3)
        ts.run(w4)
        ts.listJobs()
        ts.removeNotRun()
        println("--------------------------------------------------")
        ts.listJobs()
        println("--------------------------------------------------")
        ts.threadStatus()
        println("--------------------------------------------------")
//        TimeUnit.SECONDS.sleep(5)
        ts.threadStatus()


    }
}