package me.pixka.ya.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestSensor {

    @Autowired
    lateinit var ss:SensorService

    fun add(name:String ="Test"): Sensor {
        var s = Sensor()
        s.name=name
        return ss.save(s)
    }

    @Test
    fun testAdd()
    {
        add("Test 1")
        add("vasse")

        var list  = ss.all()

        Assertions.assertTrue(list.size>0)
    }
    @Test
    fun testFindOrCreate()
    {

        var sensor = ss.findOrCreate("for find Test sensor")
        Assertions.assertNotNull(sensor)
    }

    @Test
    fun testCount()
    {
        testAdd()
        println(ss.count())
    }
}