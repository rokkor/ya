package me.pixka.ya.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestEmail {
    @Autowired
    lateinit var es:EmailforNotifyService

    @Test
    fun addEmailnotify()
    {

        var e = es.findOrCreate("ky@pixka.me")
        Assertions.assertTrue(e.name.equals("ky@pixka.me"))

    }
}