package me.pixka.ya.d

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.util.*

@DataJpaTest
class TestfindLastValue {

    @Autowired
    lateinit var ss: SensorService

    @Autowired
    lateinit var svs: SensorValueService

    @Test
    fun testFindlastValue() {
        var s1 = ss.findOrCreate("TEst sensor for 1")
        var s2 = ss.findOrCreate("Test sensor for 2")


        var sv1 = SensorValue()
        sv1.sensor = s1
        sv1.t = 40.0
        sv1.h = 60.0
        svs.save(sv1)

        var sv2 = SensorValue()
        sv2.sensor = s2
        sv2.t = 45.0
        sv2.h = 67.0
        sv2.adddate = Date()
        svs.save(sv2)

        sv2 = SensorValue()
        sv2.sensor = s2
        sv2.t = 99.9
        sv2.h = 99.9
        sv2.adddate = Date()
        svs.save(sv2)


        var last = svs.findlast(s2.id)
        println(last)

    }
}