package me.pixka.ya.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestSearchBySensor {

    @Autowired
    lateinit var ss:SensorService
    fun add(name:String ="Test"): Sensor {
        var s = Sensor()
        s.name=name
        return ss.save(s)
    }

    @Test
    fun testSearchbyName()
    {
        add("sensor 1")
        add("sensor 2")
        add("fors")

        var s = ss.searchbyName("fors")
        println(s)
        Assertions.assertTrue(s!!.size>0)
    }
}