package me.pixka.ya.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestAddGroup {

    @Autowired
    lateinit var sgs: SensorGroupService


    @Test
    fun testAddGroup() {
        var testgroupsensor = sgs.findOrCreate("sensor group test")

        Assertions.assertTrue(testgroupsensor.name.equals("sensor group test"))

    }
}