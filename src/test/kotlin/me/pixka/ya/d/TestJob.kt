package me.pixka.ya.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestJob {

    @Autowired
    lateinit var ss:SensorService
    @Autowired
    lateinit var svs:SensorValueService

    @Autowired
    lateinit var jts:JobtypeService

    @Autowired
    lateinit var js:JobService


    var jobtype1:Jobtype?=null
    @Test
    fun testAddJob()
    {
        var s1 = ss.findOrCreate("s1")
        var s2 = ss.findOrCreate("s2")

        var jt1 = jts.findOrCreate("jobtype ")
        jobtype1 = jt1
        var jt2 = jts.findOrCreate("jobtype 2")


        var job = Job()
        job.name = "test add job"
        job.hl = 0.0
        job.ht = 100.0
        job.tl = 0.0
        job.th = 100.0
        job.jobtype =jt1
        job.jobtype_id = jt1.id
        job.sensor = s1

        var j = js.save(job)
        println(j.jobtype_id)
        Assertions.assertTrue(js.all().size>0)





    }

    @Test
    fun findJobByType()
    {

        testAddJob()
       var list =  js.findByTypeid(jobtype1!!.id)

        Assertions.assertTrue(list!!.size>0)
        println(list)
    }

}