package me.pixka.ya.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestSensorvalue {

    @Autowired
    lateinit var svs: SensorValueService

    @Autowired
    lateinit var ss: SensorService

    @Test
    fun testAdd() {

        var s1 = ss.findOrCreate("TEst sensor for 1")
        var s2 = ss.findOrCreate("Test sensor for 2")


        var sv1 = SensorValue()
        sv1.sensor = s1
        sv1.t = 40.0
        sv1.h = 60.0
        svs.save(sv1)

        var sv2 = SensorValue()
        sv2.sensor = s2
        sv2.t = 45.0
        sv2.h = 67.0
        svs.save(sv2)


        Assertions.assertTrue(svs.all().size>0)


    }

    @Test
    fun testCount()
    {
        testAdd()
        println(svs.count())
    }
}