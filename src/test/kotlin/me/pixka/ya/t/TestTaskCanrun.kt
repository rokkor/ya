package me.pixka.ya.t

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import me.pixka.ya.c.Foradddata
import me.pixka.ya.d.*
import me.pixka.ya.s.LastService
import me.pixka.ya.task.TaskService
import me.pixka.ya.w.TWorker
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestTaskCanrun {

    @Autowired
    lateinit var ss: SensorService


    @Autowired
    lateinit var jts: JobtypeService

    @Autowired
    lateinit var js: JobService

    @Autowired
    lateinit var es: EmailforNotifyService

    @Autowired
    lateinit var ts: TaskService

    fun addJob(tl: Double = 10.0, th: Double = 50.0): Job {
        var job = Job()
        job.name = "for test"
        job.jobtype = jts.findOrCreate("t")
        job.tl = tl
        job.th = th
        return js.save(job)
    }

    fun addEmail() {
        es.findOrCreate("ky@pixka.me")
    }

    @Test
    fun testSendEmail() {
        var fordata = Foradddata()
        fordata.t = 2.9
        var job = addJob()
        job.sensor = ss.findOrCreate("fortest")
        var ls = mockk<LastService>(relaxed = true)

        every { ls.findLast(job.sensor!!.id) } returns fordata
        var tw = TWorker(job, ls,es,job.id, job.name!!)
        tw.run()

        verify { ls.findLast(job.sensor!!.id) }

//        addEmail()


    }


}