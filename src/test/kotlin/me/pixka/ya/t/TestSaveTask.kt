package me.pixka.ya.t

import me.pixka.ya.c.Foradddata
import me.pixka.ya.d.SensorValueService
import me.pixka.ya.d.ServerConfig
import me.pixka.ya.d.ServerConfigService
import me.pixka.ya.s.LastService
import me.pixka.ya.task.SaveTask
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.util.concurrent.TimeUnit

@SpringBootTest
class TestSaveTask {

    @Autowired
    lateinit var cfg: ServerConfigService

    @Autowired
    lateinit var svs: SensorValueService

    @Autowired
    lateinit var ls: LastService

    @Autowired
    lateinit var ts: SaveTask

    fun addLast(l: Foradddata) {
        ls.add(l)
    }

    fun setConfig() {
        var c = cfg.findOrCreate("savetime", "1")
        c.configvalue = "1"
        cfg.save(c)
    }

    @Test
    fun testSaveTask() {

        addLast(Foradddata(5, "sensor5", 10.00, 77.0))
        addLast(Foradddata(4, "sensor4", 22.00, 88.0))
        setConfig()

        TimeUnit.SECONDS.sleep(2)
        println("Size: ${svs.all().size}")

        Assertions.assertTrue(svs.all().size>0)
        ls.print()
        addLast(Foradddata(5, "sensor5", 22.22, 33.33))
        ls.print()
    }

}