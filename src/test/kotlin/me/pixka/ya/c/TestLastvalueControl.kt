package me.pixka.ya.c

import me.pixka.ya.d.SensorService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import java.net.URI
import java.util.*
import kotlin.collections.ArrayList

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestLastvalueControl {
    @Autowired
    private val controller: GetLastValueControl? = null

    @LocalServerPort
    private val port = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    lateinit var ss: SensorService
    fun getRandomNumber(min: Int, max: Int): Int {
        return (Math.random() * (max - min) + min).toInt()
    }

    fun testAddSensorValues() {
        val baseUrl = "http://localhost:${port}/addsensorvalues/"
        val uri = URI(baseUrl)


        var list = ArrayList<Foradddata>()

        for (i in 0..1000) {
            var s = Foradddata()
            s.sensorid = ss.findOrCreate("Sensor for test control ${i}").id.toString()
            s.t = getRandomNumber(0, 100).toDouble()
            s.h = getRandomNumber(0, 100).toDouble()
            s.valuedate = Date().time
            list.add(s)
        }


        val headers = HttpHeaders()

        val request: HttpEntity<List<Foradddata>> = HttpEntity<List<Foradddata>>(list, headers)
//        println(request)
        val result = restTemplate!!.postForEntity(
            uri, request,
            String::class.java
        )
//        println(result)
    }

    @Test
    fun Testlast() {
        testAddSensorValues()
        var result = restTemplate!!.getForObject("http://localhost:${port}/lastvalues/0/10", String::class.java)
        println(result)
        result = restTemplate!!.getForObject("http://localhost:${port}/lastvalues/1/10", String::class.java)
        println(result)
    }
}