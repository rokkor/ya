package me.pixka.ya.c

import me.pixka.ya.d.SensorService
import me.pixka.ya.d.SensorValue
import me.pixka.ya.d.SensorValueService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForObject
import org.springframework.boot.web.server.LocalServerPort

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestListSensorValuesData {
    @Autowired
    lateinit var svs: SensorValueService

    @Autowired
    private val controller: ListSensorValueControl? = null

    @LocalServerPort
    private val port = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    lateinit var ss: SensorService

    fun add() {
        var s1 = ss.findOrCreate("TEst sensor for 1")
        var s2 = ss.findOrCreate("Test sensor for 2")


        var sv1 = SensorValue()
        sv1.sensor = s1
        sv1.t = 40.0
        sv1.h = 60.0
        svs.save(sv1)

        var sv2 = SensorValue()
        sv2.sensor = s2
        sv2.t = 45.0
        sv2.h = 67.0
        svs.save(sv2)

    }

    @Test
    fun testListDataFromControl() {
        add()
        var re = restTemplate!!.getForObject("http://localhost:" + port + "/listalldata", String::class.java)
        println(re)
        Assertions.assertNotNull(re)
//        restTemplate.getForObject("http://localhost:" + port + "/",String.class)
    }
}