package me.pixka.ya.c

import me.pixka.ya.d.SensorService
import me.pixka.ya.d.SensorValue
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import java.net.URI


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestAddsensorControl {

    @Autowired
    private val controller: AddSensorValueControl? = null

    @LocalServerPort
    private val port = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    lateinit var ss:SensorService


    @Test
    @Throws(Exception::class)
    fun contextLoads() {
        Assertions.assertThat(controller).isNotNull
        assertThat(
            restTemplate!!.getForObject(
                "http://localhost:$port/",
                String::class.java
            )
        ).contains("version")
    }

    @Test
    fun testAddSensorValue()
    {
        val baseUrl = "http://localhost:${port}/addsensorvalue/"
        val uri = URI(baseUrl)


        var list = ArrayList<SensorValue>()
        var s = SensorValue()
        s.sensor = ss.findOrCreate("Sensor for test control 1")
        s.t = 100.10
        s.h = 60.0

        list.add(s)

        s = SensorValue()
        s.sensor = ss.findOrCreate("Sensor for test control 2")
        s.t = 80.10
        s.h = 50.0
        list.add(s)
        val headers = HttpHeaders()

        val request: HttpEntity<List<SensorValue>> = HttpEntity<List<SensorValue>>(list, headers)
        println(request)
        val result = restTemplate!!.postForEntity(
            uri, request,
            String::class.java
        )
      println(result)
    }

    @Test
    fun testAddSensorValues()
    {
        val baseUrl = "http://localhost:${port}/addsensorvalues/"
        val uri = URI(baseUrl)


        var list = ArrayList<Foradddata>()
        var s = Foradddata()
        s.sensorid = ss.findOrCreate("Sensor for test control 1").id.toString()
        s.t = 100.10
        s.h = 60.0
        s.valuedate = 1002020
        list.add(s)

        s = Foradddata()
        s.sensorid = ss.findOrCreate("Sensor for test control 2").id.toString()
        s.t = 80.10
        s.h = 50.0
        s.valuedate =1202001
        list.add(s)
        val headers = HttpHeaders()

        val request: HttpEntity<List<Foradddata>> = HttpEntity<List<Foradddata>>(list, headers)
        println(request)
        val result = restTemplate!!.postForEntity(
            uri, request,
            String::class.java
        )
        println(result)
    }
}