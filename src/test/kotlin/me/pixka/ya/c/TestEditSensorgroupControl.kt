package me.pixka.ya.c

import me.pixka.ya.d.SensorGroup
import me.pixka.ya.d.SensorGroupService
import me.pixka.ya.d.SensorValue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestEditSensorgroupControl {

    @Autowired
    lateinit var sgs: SensorGroupService
    @LocalServerPort
    private val port = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Test
    fun testEditSensorgroup()
    {

        var edit = sgs.findOrCreate("for edit")


        edit.name = "edit for edit"
        val headers = HttpHeaders()

        val request: HttpEntity<SensorGroup> = HttpEntity<SensorGroup>(edit, headers)
        println(request)
        val result = restTemplate!!.postForEntity(
            "http://localhost:"+port+"/sensorgroup/edit", request,
            String::class.java
        )

        println(result)
    }

}