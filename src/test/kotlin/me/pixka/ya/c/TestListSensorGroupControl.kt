package me.pixka.ya.c

import me.pixka.ya.d.EmailforNotifyService
import me.pixka.ya.d.SensorGroupService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestListSensorGroupControl {

    @Autowired
    lateinit var sgs: SensorGroupService
    @LocalServerPort
    private val port = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Test
    fun testlistEmailviaControl()
    {

        var r = sgs.findOrCreate("Test group")
        sgs.findOrCreate("test group 2")
        var result = restTemplate!!.getForObject("http://localhost:${port}/sensorgroup/list", String::class.java)
        println(result)
    }
}