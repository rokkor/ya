package me.pixka.ya.c

import me.pixka.ya.d.EmailforNotifyService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestListEmail {
    @Autowired
    lateinit var es:EmailforNotifyService
    @LocalServerPort
    private val port = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Test
    fun testlistEmailviaControl()
    {

        var r = es.findOrCreate("ky@pixka.me")
        var result = restTemplate!!.getForObject("http://localhost:${port}/emaillist", String::class.java)
        println(result)
    }
}