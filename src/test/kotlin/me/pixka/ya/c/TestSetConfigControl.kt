package me.pixka.ya.c

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestSetConfigControl {
    @LocalServerPort
    private val port = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null


    @Test
    fun testCreateConfig()
    {
        var result = restTemplate!!.getForObject("http://localhost:${port}/setconfig/updatetime/5", String::class.java)
        println(result)
    }
}