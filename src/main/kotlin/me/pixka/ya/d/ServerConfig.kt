package me.pixka.ya.d

import me.pixka.ya.s.DefaultService
import me.pixka.ya.s.findByName
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import javax.persistence.Entity

@Entity
class ServerConfig(var name: String? = null, var configvalue: String? = null) : En() {
    override fun toString(): String {
        return "config:${name} value:${configvalue}"
    }
}


@Repository
interface ServerConfigRepo : JpaRepository<ServerConfig, Long>,findByName<ServerConfig>

@Service
class ServerConfigService(val r: ServerConfigRepo) : DefaultService<ServerConfig>()
{
    @Synchronized
    fun findOrCreate(n:String,v:String="0"): ServerConfig {
        var found = findByName(n)
        if(found==null)
        {
            var scfg = ServerConfig()
            scfg.name = n
            scfg.configvalue = v
            return save(scfg)
        }
        return found
    }
}

