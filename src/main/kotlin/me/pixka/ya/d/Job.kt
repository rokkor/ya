package me.pixka.ya.d

import me.pixka.ya.s.DefaultService
import me.pixka.ya.s.findByName
import me.pixka.ya.s.search
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne

@Entity
class Job(
    var name: String? = null,
    @ManyToOne var jobtype: Jobtype? = null,
    @Column( insertable = false, updatable = false) var jobtype_id: Long? = null,
    var hl: Double? = null,var ht:Double?=null,
    var tl: Double? = null,var th:Double?=null,
    var rt:Long?=null,var wt:Long?=null,
    @ManyToOne var sensor:Sensor?=null,
    @Column(insertable = false, updatable = false) var sensor_id:Long?=null,

   var token:String?=null,var enable:Boolean?=false
) : En() {
    override fun toString(): String {
        return "name:${name}   jobtype ${jobtype} Job type_id ${jobtype_id}"
    }
}

@Repository
interface JobRepo : JpaRepository<Job, Long>,search<Job>,findByName<Job>
{
    fun findByJobtype_id(id:Long):List<Job>?

    @Query("from Job j where j.name like %?1% ")
    override fun search(s: String, page: Pageable): List<Job>?
}

@Service
class JobService(val r: JobRepo) : DefaultService<Job>()
{
    fun findByTypeid(id:Long)=r.findByJobtype_id(id)
    fun search(s:String,page:Long,limit:Long)=r.search(s,toPage(page.toInt(),limit.toInt()))
}