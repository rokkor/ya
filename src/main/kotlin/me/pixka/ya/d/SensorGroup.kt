package me.pixka.ya.d

import me.pixka.ya.s.DefaultService
import me.pixka.ya.s.findByName
import me.pixka.ya.s.search
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import javax.persistence.Entity

@Entity
class SensorGroup (var name:String?=null,var description:String?=null):En()
{

}

@Repository
interface SensorGroupRepo:JpaRepository<SensorGroup,Long>,findByName<SensorGroup>,search<SensorGroup>
{
    @Query("from SensorGroup sg where sg.name like %?1%")
    override fun search(s: String, page: Pageable): List<SensorGroup>?
}

@Service
class SensorGroupService(val r:SensorGroupRepo):DefaultService<SensorGroup>()
{
    @Synchronized
    fun findOrCreate(n:String="defaultgroup"): SensorGroup {
        var sg = findByName(n)
        if(sg==null)
        {
            var nsg = SensorGroup(n,"auto add")
            return  save(nsg)
        }

        return sg
    }
    fun searchByname(n:String)=r.search(n,toPage())
}