package me.pixka.ya.d

import me.pixka.ya.s.DefaultService
import me.pixka.ya.s.findByName
import me.pixka.ya.s.search
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import javax.persistence.*

@Entity
class Sensor(
    var name: String? = null,
    var description: String? = null,
    @ManyToOne var sensorgroup: SensorGroup? = null,
    @Column(insertable = false, updatable = false) var sensorgroup_id: Long? = null
) : En() {

}

@Repository
interface SensorRepo : JpaRepository<Sensor, Long>, findByName<Sensor>, search<Sensor> {


    @org.springframework.data.jpa.repository.Query("from Sensor s where s.name like %?1%")
    override fun search(s: String, page: Pageable): List<Sensor>?

}

@Service
class SensorService(val r: SensorRepo) : DefaultService<Sensor>() {
    @Synchronized
    fun findOrCreate(n: String): Sensor {
        var sensor = findByName(n)
        if (sensor == null) {
            var s = Sensor(n)
            return save(s)
        }

        return sensor
    }

    fun searchbyName(n: String): List<Sensor>? {
        return r.search(n, Pageable.ofSize(100))
    }
}