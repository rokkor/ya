package me.pixka.ya.d

import me.pixka.ya.s.DefaultService
import me.pixka.ya.s.findByName
import me.pixka.ya.s.search
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.util.*
import javax.persistence.Entity

@Entity
class Jobtype(var name: String? = null) : En() {
    override fun toString(): String {
        return "id:${id} name:${name}"
    }
}

@Repository
interface JobtypeRepo:JpaRepository<Jobtype,Long>,search<Jobtype>,findByName<Jobtype>
{
    @Query("from Jobtype j where j.name like %?1%")
    override fun search(s: String, page: Pageable): List<Jobtype>?

}

@Service
class JobtypeService(val r:JobtypeRepo):DefaultService<Jobtype>()
{
    @Synchronized
    fun findOrCreate(n:String): Jobtype {
        var found = r.findByName(n)
        if(found==null)
        {
            return save(Jobtype(n))
        }

        return found

    }

    fun searchByDate(s:Date,e: Date)=r.search("",toPage(0,1000))
    fun search(s:String,page:Long,limit:Long) = r.search(s,toPage(page.toInt(), limit.toInt()))
}