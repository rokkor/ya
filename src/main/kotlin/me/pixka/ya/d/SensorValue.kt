package me.pixka.ya.d

import me.pixka.ya.s.DefaultService
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne

@Entity
class SensorValue(@ManyToOne var sensor:Sensor?=null,
                  @Column(insertable = false, updatable = false) var sensor_id:Long?=null,
                var t:Double?=null,var h:Double?=null,var valuedate:Date?=null
                  ):En() {
    override fun toString(): String {
        return "${sensor!!.name } T:${t} H${h}"
    }
}

@Repository
interface SensorValueRepo:JpaRepository<SensorValue,Long>
{
    fun findTopSensorValueBySensor_IdOrderByAdddateDesc(id:Long):SensorValue?

    @Query("from SensorValue sv where sv.sensor_id=?1 and  sv.adddate  >= ?2  and  sv.adddate <=?3")
    fun findByDate(id:Long,s:Date,e:Date):List<SensorValue>?
    @Query("from SensorValue sv where   sv.adddate  >= ?1  and  sv.adddate <=?2")
    fun findByDate(s:Date,e:Date):List<SensorValue>?

}

@Service
class SensorValueService(val r:SensorValueRepo):DefaultService<SensorValue>()
{
    fun findlast(id:Long)=r.findTopSensorValueBySensor_IdOrderByAdddateDesc(id)
    fun findByDate(id:Long,s:Date,d:Date)=r.findByDate(id,s,d)
    fun findByDate(s:Date,d:Date)=r.findByDate(s,d)
}