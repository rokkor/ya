package me.pixka.ya.task

import me.pixka.ya.d.SensorService
import me.pixka.ya.d.SensorValue
import me.pixka.ya.d.SensorValueService
import me.pixka.ya.d.ServerConfigService
import me.pixka.ya.s.LastService
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.*

//สำหรับ Save task
@Service
class SaveTask(val cfg: ServerConfigService,val ls:LastService,val ss: SensorService,val svs: SensorValueService) {

    var t = 0

    @Scheduled(fixedDelay = 1000)
    fun run() {
        t++
        var f = cfg.findOrCreate("savetime", "300") //default 300 5 min
        if(t>= f.configvalue!!.toInt())
        {
            println("Save time")
            t=0
            save()
        }
    }

    fun save() {
        ls.lastvalues.forEach {
            var sv = SensorValue()
            sv.sensor = ss.findOrCreate(it.sensorid!!)
            sv.h = it.h
            sv.t = it.t
            if (it.valuedate != null) {
                sv.adddate = Date(it.valuedate!! * 1000)
                sv.valuedate = sv.adddate
            } else {
                sv.adddate = Date()
                sv.valuedate = sv.adddate
            }

            svs.save(sv)
        }
    }
}