package me.pixka.ya.task

import me.pixka.ya.d.*
import me.pixka.ya.s.LastService
import me.pixka.ya.w.TWorker
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class LoadTjobTask(val jts: JobtypeService, val js: JobService,
                   val ls:LastService,val ts: TaskService, val svs: SensorValueService,val es:EmailforNotifyService) {


    @Scheduled(fixedDelay = 1000)
    fun run() {
        var jt = jts.findOrCreate("t")
        var jobs = js.findByTypeid(jt.id)
        if (jobs != null) {
            jobs.forEach {

                try {
                    if (checkT(it)) {
                        var t = TWorker(it,ls,es,it.id, it.name!!)
                        ts.run(t)
                    }
                }catch (e:Exception)
                {
                    e.printStackTrace()
                }
            }
        }

    }

    /**
     * check tmp
     */
    fun checkT(job: Job): Boolean {
        try {
            var tl = job.tl!!.toDouble()
            var th = job.th!!.toDouble()
            var lastvalue = svs.findlast(job.sensor_id!!)!!.t!!.toDouble()

            if (tl <= lastvalue && lastvalue <= th) {
                return true
            }

            return false
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }

}