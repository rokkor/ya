package me.pixka.ya.task

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor

@Service
class TaskService {
    var runinglist = CopyOnWriteArrayList<JobInterface>()
    var threads = Executors.newFixedThreadPool(50)

    fun isRun(job: JobInterface): JobInterface? {
        return runinglist.find { it.getJobId() == job.getJobId() }
    }

    fun run(job: JobInterface): Boolean {
        if (isRun(job) == null) {
            runinglist.add(job)
            threads.execute(job as Runnable)
            return true

        }

        return false

    }

    @Scheduled(fixedDelay = 5000)
    fun removeNotRun() {
        runinglist.forEach {

            if (!it.isRun())
                runinglist.remove(it)
        }
    }

    fun threadStatus() {
        var p = threads as ThreadPoolExecutor
        println("Task Count: ${p.taskCount}")
        println("Active: ${p.activeCount}")
        println("Queue: ${p.queue}")
    }

    fun listJobs() {
        runinglist.forEach { println(it) }
    }
}

interface JobInterface {
    fun getJobId(): Long
    fun getJobName(): String
    fun isRun(): Boolean


}