package me.pixka.ya

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
@EnableAsync
@ComponentScan(basePackages = arrayOf("me.pixka"))
class YaApplication

fun main(args: Array<String>) {
	runApplication<YaApplication>(*args)
}
