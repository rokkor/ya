package me.pixka.ya.o

import java.util.*

class SearchObj(var search:String?=null,
                var id:Long?=null, var sdate: Date?=null, var edate:Date?=null,
                var page:Long=0,var limit:Long=1000,var name:String?=null) {
    override fun toString(): String {
        return "ID${id} SDATE:${sdate} EDATE:${edate}"
    }
}