package me.pixka.ya.c

import me.pixka.ya.d.EmailforNotify
import me.pixka.ya.d.EmailforNotifyService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ListEmailControl(val es:EmailforNotifyService) {


    @CrossOrigin
    @GetMapping("/emaillist")
    fun getemail(): MutableList<EmailforNotify> {
        return es.all()
    }

}