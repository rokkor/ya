package me.pixka.ya.c

import me.pixka.ya.s.LastService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class GetLastBySensorgroupIdControl(val ls:LastService) {

    @GetMapping("/last/bygroupid/{id}")
    @CrossOrigin
    fun getlastbysensorgroupid(@PathVariable("id")id:Long): List<Foradddata> {
        return ls.findByGroup(id)
    }
}