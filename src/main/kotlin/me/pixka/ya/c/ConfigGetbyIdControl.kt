package me.pixka.ya.c

import me.pixka.ya.d.ServerConfig
import me.pixka.ya.d.ServerConfigService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class ConfigGetbyIdControl(val scf:ServerConfigService) {


    @CrossOrigin
    @GetMapping("/config/get/{id}")
    fun get(@PathVariable("id") id:Long): ServerConfig? {
        var found =  scf.findById(id)
        if(found.isPresent)
            return found.get()

        return null
    }
}