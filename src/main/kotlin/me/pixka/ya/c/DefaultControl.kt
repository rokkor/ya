package me.pixka.ya.c

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class DefaultControl {

    @GetMapping("/")
    fun d(): String {
        return "{\"version\":\"0.2\"}"
    }
}