package me.pixka.ya.c

import me.pixka.ya.d.ServerConfig
import me.pixka.ya.d.ServerConfigService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ConfigEditControl(val scgs:ServerConfigService) {

    @CrossOrigin
    @PostMapping("/config/edit")
    fun edit(@RequestBody config:ServerConfig): ServerConfig {
        return scgs.save(config)
    }
}