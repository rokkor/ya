package me.pixka.ya.c

import me.pixka.ya.d.SensorValueService
import me.pixka.ya.o.SearchObj
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

@RestController
class ExportSensorValuesControl(val svs: SensorValueService) {

    @PostMapping("/exportsensorvalues")
    @CrossOrigin
    fun exportallsensorvaluebyDate(@RequestBody s: SearchObj): ResponseEntity<InputStreamResource> {
        try {
            var t = svs.findByDate( s.sdate!!, s.edate!!)
            val wb: Workbook = XSSFWorkbook()
            val sheet: Sheet = wb.createSheet("new sheet")
            val bo = ByteArrayOutputStream()
            var rows = 1
            var f = sheet.createRow(0)

            var cell = f.createCell(0)
            cell.setCellValue("record id")
            cell = f.createCell(1)
            cell.setCellValue("sensor name")
            cell = f.createCell(2)
            cell.setCellValue("value dete")
            cell = f.createCell(3)
            cell.setCellValue("t")

            cell = f.createCell(4)
            cell.setCellValue("h")

            if (t != null) {
                t.forEach {
                    var row = sheet.createRow(rows)
                    var cell = row.createCell(0)
                    cell.setCellValue(it.id.toDouble())
                    cell = row.createCell(1)
                    cell.setCellValue(it.sensor!!.name)
                    cell = row.createCell(2)
                    cell.setCellValue(it.adddate)
                    if (it.t != null) {
                        cell = row.createCell(3)
                        cell.setCellValue(it.t!!)
                    }
                    if (it.h != null) {
                        cell = row.createCell(4)
                        cell.setCellValue(it.h!!)
                    }
                    rows++
                }
            }

            wb.write(bo)

            val respHeaders = HttpHeaders()
            respHeaders.contentType = MediaType.APPLICATION_FORM_URLENCODED
            respHeaders.contentLength = bo.size().toLong()
            respHeaders.setContentDispositionFormData("attachment", "export.xlsx")
//            val isr = InputStreamResource(FileInputStream(f))
            val isr = InputStreamResource(ByteArrayInputStream(bo.toByteArray()))
            return ResponseEntity(isr, respHeaders, HttpStatus.OK)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }
}