package me.pixka.ya.c

import me.pixka.ya.d.SensorValue
import me.pixka.ya.d.SensorValueService
import me.pixka.ya.o.SearchObj
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class GetSensorValueControl(val svs: SensorValueService) {


    @CrossOrigin
    @PostMapping("/getsensorvalue")
    fun getsensorValue(@RequestBody s: SearchObj): List<SensorValue>? {

        println(s)
        try {
            return svs.findByDate(s.id!!, s.sdate!!, s.edate!!)
        } catch (e: Exception) {
            throw e
        }
    }
}