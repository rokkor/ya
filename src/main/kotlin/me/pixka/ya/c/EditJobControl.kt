package me.pixka.ya.c

import me.pixka.ya.d.Job
import me.pixka.ya.d.JobService
import me.pixka.ya.d.JobtypeService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class EditJobControl(val js: JobService, val jts: JobtypeService) {

    @PostMapping("/job/edit")
    @CrossOrigin
    fun edit(@RequestBody job: Job): Job {
        job.jobtype = jts.findOrCreate(job.jobtype?.name!!)
        return js.save(job)
    }
}