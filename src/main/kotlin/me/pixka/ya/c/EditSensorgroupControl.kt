package me.pixka.ya.c

import me.pixka.ya.d.SensorGroup
import me.pixka.ya.d.SensorGroupService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class EditSensorgroupControl(val sgs: SensorGroupService) {

    @CrossOrigin
    @PostMapping("/sensorgroup/edit")
    fun edit(@RequestBody sensorgroup: SensorGroup): SensorGroup {
       return sgs.save(sensorgroup)
    }

}