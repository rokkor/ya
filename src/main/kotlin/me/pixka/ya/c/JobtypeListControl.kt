package me.pixka.ya.c

import me.pixka.ya.d.Jobtype
import me.pixka.ya.d.JobtypeService
import me.pixka.ya.o.SearchObj
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class JobtypeListControl(val jts:JobtypeService) {

    @CrossOrigin
    @PostMapping("/jobtype/sn")
    fun sn(@RequestBody s:SearchObj): List<Jobtype>? {
      return  jts.search(s.name!!,s.page,s.limit)
    }
}