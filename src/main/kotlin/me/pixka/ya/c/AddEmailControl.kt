package me.pixka.ya.c

import me.pixka.ya.d.EmailforNotify
import me.pixka.ya.d.EmailforNotifyService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class AddEmailControl(val es:EmailforNotifyService) {

    @CrossOrigin
    @GetMapping("/addemail/{email}")
    fun addemail(@PathVariable("email") email:String): EmailforNotify {
        return es.findOrCreate(email)
    }
}