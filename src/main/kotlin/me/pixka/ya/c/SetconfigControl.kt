package me.pixka.ya.c

import me.pixka.ya.d.ServerConfig
import me.pixka.ya.d.ServerConfigService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class SetconfigControl(val scfgs:ServerConfigService) {


    @CrossOrigin
    @GetMapping("/setconfig/{configname}/{value}")
    fun setConfig(@PathVariable("configname") cfgname:String,@PathVariable("value") value:String): ServerConfig {
        return scfgs.findOrCreate(cfgname,value)
    }
}