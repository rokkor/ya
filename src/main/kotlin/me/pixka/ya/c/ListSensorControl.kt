package me.pixka.ya.c

import me.pixka.ya.d.Sensor
import me.pixka.ya.d.SensorService
import me.pixka.ya.o.SearchObj
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ListSensorControl (val ss:SensorService) {

    @CrossOrigin
    @PostMapping("/sensor/sn")
    fun sn(@RequestBody s:SearchObj): List<Sensor>? {
        return ss.searchbyName(s.name!!)
    }
    @CrossOrigin
    @GetMapping("/sensorlist")
    fun list(): MutableList<Sensor> {
        return ss.all()
    }

    @CrossOrigin
    @GetMapping("/sensorcount")
    fun count(): Long {
        return ss.count()
    }
}