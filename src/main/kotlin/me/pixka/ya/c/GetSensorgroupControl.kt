package me.pixka.ya.c

import me.pixka.ya.d.SensorGroup
import me.pixka.ya.d.SensorGroupService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class GetSensorgroupControl(val sgs: SensorGroupService) {


    @GetMapping("/sensorgroup/get/{id}")
    @CrossOrigin
    fun getSensorgroup(@PathVariable("id") id: Long): SensorGroup? {
        var have = sgs.findById(id)
        if (have.isPresent)
            return have.get()
        return null
    }

}