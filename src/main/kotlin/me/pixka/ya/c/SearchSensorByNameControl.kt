package me.pixka.ya.c

import me.pixka.ya.d.Sensor
import me.pixka.ya.d.SensorService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class SearchSensorByNameControl(val ss: SensorService) {

    @CrossOrigin
    @GetMapping("/searchsensor/{s}")
    fun searchbyname(@PathVariable s: String): List<Sensor>? {
        return ss.searchbyName(s)
    }
}