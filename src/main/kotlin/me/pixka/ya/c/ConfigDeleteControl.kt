package me.pixka.ya.c

import me.pixka.ya.d.ServerConfigService
import org.springframework.data.jpa.repository.Modifying
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.transaction.Transactional

@RestController
class ConfigDeleteControl(val scfgs:ServerConfigService) {

    @CrossOrigin
    @Modifying
    @Transactional
    @GetMapping("/config/delete/{id}")
    fun delete(@PathVariable("id") id:Long)
    {
        return scfgs.deleteById(id)
    }
}