package me.pixka.ya.c

import me.pixka.ya.d.Sensor
import me.pixka.ya.d.SensorService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class GetSensorControl(val ss:SensorService) {

    @CrossOrigin
    @GetMapping("/getsensor/{id}")
    fun getSensor(@PathVariable("id")id:Long): Sensor? {
        var s= ss.r.findById(id)
        if(s.isPresent)
            return s.get()

        return null
    }
}