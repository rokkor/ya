package me.pixka.ya.c

import me.pixka.ya.d.EmailforNotify
import me.pixka.ya.d.EmailforNotifyService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class EditEmailControl(val es:EmailforNotifyService) {

    @CrossOrigin
    @PostMapping("/email/edit")
    fun edit(@RequestBody email: EmailforNotify): EmailforNotify {
       return  es.save(email)
    }
}