package me.pixka.ya.c

import me.pixka.ya.d.Job
import me.pixka.ya.d.JobService
import me.pixka.ya.o.SearchObj
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class JoblistControl(val js:JobService) {

    @CrossOrigin
    @PostMapping("/job/sn")
    fun sn(@RequestBody s:SearchObj): List<Job>? {
        return js.search(s!!.search!!,s.page,s.limit)
    }
}