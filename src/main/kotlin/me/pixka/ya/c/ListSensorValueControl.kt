package me.pixka.ya.c

import me.pixka.ya.d.SensorValue
import me.pixka.ya.d.SensorValueService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ListSensorValueControl (val svs:SensorValueService) {

    @CrossOrigin
    @GetMapping("/listalldata")
    fun getAlldata(): MutableList<SensorValue> {
        return svs.all()
    }
    @CrossOrigin
    @GetMapping("/datacount")
    fun count(): Long {
        return svs.count()
    }
}