package me.pixka.ya.c

import me.pixka.ya.d.Sensor
import me.pixka.ya.d.SensorGroupService
import me.pixka.ya.d.SensorService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class EditsensorControl(val ss:SensorService,val sgs:SensorGroupService) {

    @CrossOrigin
    @PostMapping("/sensoredit")
    fun editsensor(@RequestBody s:Sensor): Sensor {

        var sg = s.sensorgroup

        s.sensorgroup = sgs.findOrCreate(sg!!.name!!)

        return ss.save(s)
    }
}