package me.pixka.ya.c

import me.pixka.ya.d.Sensor
import me.pixka.ya.d.SensorService
import me.pixka.ya.d.SensorValue
import me.pixka.ya.d.SensorValueService
import me.pixka.ya.s.LastService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class AddSensorValueControl(val svs: SensorValueService, val ss: SensorService, val ls: LastService) {


    @PostMapping("/addsensorvalue")
    fun add(@RequestBody values: List<SensorValue>): String {
        println(values)
        values.forEach {
            svs.save(it)
        }

        return "OK"
    }

    @PostMapping("/addsensorvalues")
    fun adds(@RequestBody values: List<Foradddata>): String {
        println("Value size:${values.size}")
//        values.forEach { println(it) }
        values.forEach {
            try {
                if (it.id != null) {
                    var o = ss.findById(it.id!!)
                    if (o.isPresent) {
                        it.sensor = o.get()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            ls.add(it)
        }
        println("Buffer size ${ls.lastvalues.size}")
        return "OK"
    }
}

class Foradddata(
    var id: Long? = null,
    var sensorid: String? = null,
    var t: Double? = null,
    var h: Double? = null,
    var valuedate: Long? = null,
    var sensor: Sensor? = null
) {
    override fun toString(): String {
        return "id:${id} Sensor:${sensorid} T:${t} H:${h} valuedate:${valuedate}"
    }
}
