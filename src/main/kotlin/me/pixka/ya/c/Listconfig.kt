package me.pixka.ya.c

import me.pixka.ya.d.ServerConfig
import me.pixka.ya.d.ServerConfigService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class Listconfig(val scs:ServerConfigService) {

    @CrossOrigin
    @GetMapping("/configlist")
    fun configlist(): MutableList<ServerConfig> {
        return scs.all()
    }
}