package me.pixka.ya.c

import me.pixka.ya.s.LastService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class GetLastValueControl(val ls: LastService) {
    @CrossOrigin
    @GetMapping("/lastvalues/{page}/{limit}")
    fun getupdate(@PathVariable("page") page: Int, @PathVariable("limit") limit: Int): MutableList<Foradddata> {

        var start = page * limit
        var list = ls.lastvalues
        if ((start+limit) > list.size)
            return list.subList(start, list.size)

        return list.subList(start, (start+limit))
    }
}