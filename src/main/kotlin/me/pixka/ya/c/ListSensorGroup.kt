package me.pixka.ya.c

import me.pixka.ya.d.SensorGroup
import me.pixka.ya.d.SensorGroupService
import me.pixka.ya.o.SearchObj
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ListSensorGroup(val sgs:SensorGroupService) {

    @CrossOrigin
    @GetMapping("/sensorgroup/list")
    fun getGroups(): MutableList<SensorGroup> {
        return sgs.all()
    }
    @CrossOrigin
    @PostMapping("/sensorgroup/sn")
    fun sn(@RequestBody s:SearchObj): List<SensorGroup>? {
       return  sgs.searchByname(s.name!!)
    }
}