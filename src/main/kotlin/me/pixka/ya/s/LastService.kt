package me.pixka.ya.s

import me.pixka.ya.c.Foradddata
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList

@Service
class LastService {

    var lastvalues = CopyOnWriteArrayList<Foradddata>()


    fun add(n: Foradddata) {
        var found = lastvalues.find { it.id == n.id }
        if (found == null) {
            lastvalues.add(n)
        } else {
            found.h = n.h
            found.t = n.t
            if (n.valuedate != null)
                found.valuedate = n.valuedate
            else
                found.valuedate = Date().time
        }
    }

    fun findLast(id:Long): Foradddata? {
        var  found = lastvalues.find { it.id == id }
        return found
    }

    fun findByGroup(gid:Long): List<Foradddata> {

        var lastingroups =  lastvalues.filter {
            it.sensor?.sensorgroup_id == gid }
        return lastingroups
    }
    fun getLastupdate(): CopyOnWriteArrayList<Foradddata> {
        return lastvalues
    }

    fun print()
    {
        lastvalues.forEach {
            println(it)
        }
    }

}