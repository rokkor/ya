package me.pixka.ya.w

import me.pixka.ya.task.JobInterface
import java.util.concurrent.TimeUnit

open class BaseWorker(var id: Long, var name: String, var runtime: Long = 10, var waittime: Long = 0) : JobInterface,
    Runnable {
    var run = false;
    override fun getJobId(): Long {
        return id
    }

    override fun getJobName(): String {
        return name
    }

    override fun isRun(): Boolean {
        return run
    }

    override fun run() {
        run = true
        println("Base worker run ${name}")

        TimeUnit.SECONDS.sleep(runtime)
        TimeUnit.SECONDS.sleep(waittime)
        run=false
    }

    override fun toString(): String {
        return "Job ${name}  is run ${run}"
    }
}